# Open Systems Interconnection (OSI) Model
---
## The OSI model is a conceptual model of how the Internet works.
![OSI Illustration](/g1_networking_images/OSI.jpg)
---
# TCP/IP Model
![OSI Model to TCP/IP Model Illustration](/g1_networking_images/tcp.jpg)

# Data and Information
## How Data is reffered in each level.
![Data and Information](/g1_networking_images/Data.jpg)

# What is a port?
* A port is a virtual point where network connections start and end.<br>
* Ports are software-based and managed by a computer's operating system.<br>
* Each port is associated with a specific process or service.
* Ports allow computers to easily differentiate between different kinds of traffic: emails go to a different port than webpages, for instance, even though both reach a computer over the same Internet connection.<br>


##  What is a port number?
* Ports are standardized across all network-connected devices, with each port assigned a number.<br>
* Most ports are reserved for certain protocols — for example, all        Hypertext Transfer Protocol (HTTP) messages go to port 80. While IP addresses enable messages to go to and from specific devices, port numbers allow targeting of specific services or applications within those devices.

## How do ports make network connections more efficient?
* Vastly different types of data flow to and from a computer over the same network connection.<br>
* The use of ports helps computers understand what to do with the data they receive.<br>
* Ports are a transport layer (layer 4) concept. Only a transport protocol such as the Transmission Control Protocol (TCP) or User Datagram Protocol (UDP) can indicate which port a packet should go to.


## What are the different port numbers?
* There are 65,535 possible port numbers, although not all are in common use. 
    
* The numbers from 1024 to 49151 are called registered ports and can be registered with the Internet Assigned Numbers Authority for a specific use. 

* The numbers 49152 to 65535 are unassigned, can be used by any type of service and are called dynamic ports, private ports or ephemeral ports.

* Some of the most commonly used ports, along with their associated networking protocol, are:

**Ports 20 and 21:** File Transfer Protocol (FTP). FTP is for transferring files between a client and a server.

**Port 22:** Secure Shell (SSH). SSH is one of many tunneling protocols that create secure network connections.

**Port 25:** Simple Mail Transfer Protocol (SMTP). SMTP is used for email.

**Port 53:** Domain Name System (DNS). DNS is an essential process for the modern Internet; it matches human-readable domain names to machine-readable IP addresses, enabling users to load websites and applications without memorizing a long list of IP addresses.

**Port 80:** Hypertext Transfer Protocol (HTTP). HTTP is the protocol that makes the World Wide Web possible.

**Port 123:** Network Time Protocol (NTP). NTP allows computer clocks to sync with each other, a process that is essential for encryption.

**Port 179:** Border Gateway Protocol (BGP). BGP is essential for establishing efficient routes between the large networks that make up the Internet (these large networks are called autonomous systems). Autonomous systems use BGP to broadcast which IP addresses they control.

**Port 443:** HTTP Secure (HTTPS). HTTPS is the secure and encrypted version of HTTP. All HTTPS web traffic goes to port 443. Network services that use HTTPS for encryption, such as DNS over HTTPS, also connect at this port.

**Port 500:** Internet Security Association and Key Management Protocol (ISAKMP), which is part of the process of setting up secure IPsec connections.

**Port 3389:** Remote Desktop Protocol (RDP). RDP enables users to remotely connect to their desktop computers from another device.

---
# Network Protocol
### Meaning
* In networking, a protocol is a standardized way of doing certain actions and formatting data so that two or more devices are able to communicate with and understand each other.

* There is an agreed-upon protocol for writing addresses in order for the postal system to work. In the same way, all IP data packets must present certain information in a certain order, and all IP addresses follow a standardized format.

* There are various types of protocols that support a major and compassionate role in communicating with different devices across the network. These are:

**1. Internet Protocol (IP)** 

**2. Trasport Protocols**
* Transmission Control Protocol (TCP)
* User Datagram Protocol (UDP)

**3.  Application Protocols**
* Hyper Text Transfer Protocol (HTTP) - to reach webpages
* Hyper Text Transfer Protocol Secure (HTTPS)- Secure communication
* Secure Sockets Layer (SSL) - securing and safeguarding communications between systems by using encryption.
* Transport Layer Security (TLS) - updated version of SSL that is more secure.        
        
**4. Mail Protocols**
* Simple mail transport Protocol (SMTP) - transfer email messages between mail servers.
* Post office Protocol (POP) - used by email clients such as Mozilla Thunderbird or Microsoft Outlook to retrieve email messages from the mail server.
* Internet Message Access Protocol (IMAP) - same as above

**5. Remote Desktop Protocols**
* Remote Desktop Protocol (RDP) - used to access the desktop of a remote Microsoft Windows computer.
* Secure Shell Protocol (SSH) - opens a secure command line interface (CLI) on a remote Linux or Unix computer.

**6. Management and Supprt Protocols**
* Domain Name Sysytem (DNS) - database for Domain names.
* Dynamic Host Configuration Protocol (DHCP) - automatically assign IP addresses, Subnet Masks, gateways.
* Internet Control Message Protocol (ICMP) - to diagnose network communication issues.
* File Transfer Protocol (FTP) - transfer of files from one computer to another.

---
# Internet Protocol (IP)
* The Internet Protocol (IP) is a protocol, or set of rules, for routing and addressing packets of data so that they can travel across networks and arrive at the correct destination.
    
* Data traversing the Internet is divided into smaller pieces, called packets.
    
* IP information is attached to each packet, and this information helps routers to send packets to the right place.
    
* Every device or domain that connects to the Internet is assigned an IP address, and as packets are directed to the IP address attached to them, data arrives where it is needed.

* Once the packets arrive at their destination, they are handled differently depending on which transport protocol is used in combination with IP. The most common transport protocols are TCP and UDP.<br>
 
![Data Transfer](/g1_networking_images/Data_Transfer.jpg)

## IP Address
* An IP address is a unique identifier assigned to a device or domain that connects to the Internet.
* Each IP address is a series of characters, such as '192.168.1.1'.
* Via DNS resolvers, which translate human-readable domain names into IP addresses, users are able to access websites without memorizing this complex series of characters.
* Each IP packet will contain both the IP address of the device or domain sending the packet and the IP address of the intended recipient, much like how both the destination address and the return address are included on a piece of mail.

## IP Packet?
* IP packets are created by adding an IP header to each packet of data before it is sent on its way.
* An IP header is just a series of bits (ones and zeros), and it records several pieces of information about the packet, including the sending and receiving IP address.
    
* IP headers also report:<br>
            •	Header length<br>
            •	Packet length<br>
            •	Time To Live (TTL)<br>
            •	Which transport protocol is being used (TCP, UDP, etc.)

## TCP/IP
* The Transmission Control Protocol (TCP) is a transport protocol, meaning it dictates the way data is sent and received.
* A TCP header is included in the data portion of each packet that uses TCP/IP. Before transmitting data, TCP opens a connection with the recipient.
* TCP ensures that all packets arrive in order once transmission begins. Via TCP, the recipient will acknowledge receiving each packet that arrives. Missing packets will be sent again if receipt is not acknowledged.
* TCP is designed for reliability, not speed. Because TCP has to make sure all packets arrive in order, loading data via TCP/IP can take longer if some packets are missing.
* TCP and IP were originally designed to be used together, and these are often referred to as the TCP/IP suite. However, other transport protocols can be used with IP.

## UDP/IP
* The User Datagram Protocol, or UDP, is another widely used transport protocol.
* It's faster than TCP, but it is also less reliable.
* UDP does not make sure all packets are delivered and in order, and it doesn't establish a connection before beginning or receiving transmissions.

![TCP_UDP](/g1_networking_images/TCP_UDP.jpg)

* TCP and UDP headers have a section for indicating port numbers. Network layer protocols — for instance, the Internet Protocol (IP) — are unaware of what port is in use in a given network connection. In a standard IP header, there is no place to indicate which port the data packet should go to. IP headers only indicate the destination IP address, not the port number at that IP address.
---
# SERVER
A server is a computer program or device that provides a service to another computer program and its user, also known as the client.

![Data Transfer](/g1_networking_images/server.jpg)


## TYPES OF SERVERS

### 1. Web Server
* A web server powers the site you’re looking at right now. This genre of server focuses on serving web content to clients.<br> 
* Web servers are typically “headless” in nature. This is to preserve the memory on the server and ensure that there’s enough to power the operating system and applications on the server.<br> 
* “Headless” means that it doesn’t run like a traditional home computer, but rather just serves content. The administrators of these servers can only connect to them through command line terminals.<br> 
* They can also run on any operating system, as long as they obey the general “rules” of the web.<br> 
* A web browser then renders the content accordingly to show the page as you’re reading it now.<br> 
* Some popular webservers include Microsoft IIS, Apache, Nginx etc.<br> 
* Some Ports used for Webservers: Port 80 for HTTP (not encrypted) and Port 443 for HTTPs (encrypted).<br> 

### 2. Database Server
* A database server typically to store data in groups.<br> 
* One of the more common method of keeping data is known as “SQL” or “Structured Query Language”.<br> 
* Web applications usually have their server-side components connect to a Database server to grab data as users request it.<br> 
* A good practice is to have webservers and database servers on different machines. The reason that database servers should exist on their own is for security.<br> 
* Some popular Database servers include MySQL, MariaDB, Microsoft SQL, Oracle Database etc.<br> 
* Some Ports used for Database Servers: Port 3306 (MySQL, MariaDB), Port 1433 (MS-SQL), Port 1521 (Oracle DB).<br>

### 3. eMail Server
* An email server typically runs on the dominant protocol “SMTP” or “Simple Mail Transfer Protocol”.<br> 
* An email server powers mail services. These servers in themselves simply take in emails from one client to another and forward the data to the other server.<br> 
* The modern approach to email servers typically pairs them with web servers.<br> 
* Some Ports used for eMail Servers: Port 25 (SMTP), Port 587 (Secure SMTP), Port 110 (POP3)<br>

### 4. Web Proxy Server
* A web proxy server can run on one of many protocols, but they all do one thing in common.<br> 
* They take in user requests, filter them, and then act on the user’s behalf. The most popular type of web proxy server is designed to get around school and organizational web filters.<br> 
* Because web traffic is all through one IP address and website that isn’t yet blocked, users can gain access to sites that are forbidden through these filters.<br> 
* Some Ports used for Web Proxy Servers: Port 8080, 8888 etc.<br>

### 5. DNS Server
* A DNS server, or “Domain Name Service” server, is used to translate domain names to their corresponding IP addresses.<br> 
* This server is what your browser references when you type in a domain name and press Enter. The idea is that users don’t have to memorize IP addresses and organizations can have a fitting name.<br> 
* Typically, Internet Service Providers (ISPs) provide DNS servers to their users.<br> 
* The domain name is registered with one higher-up DNS server that other, lower-level DNS servers reference.<br> 
* Ports used for DNS Servers: Port 53 (both TCP and UDP).<br>

### 6. FTP Server
* FTP servers, or “File Transfer Protocol” servers, have a single purpose: to host a file exchange among users.<br> 
* This type of server allows users to upload files to it or download files after authenticating through an FTP client.<br> 
* Users can also browse the server’s files and download individual files as they wish.<br> 
* Some Ports used for FTP Servers: Ports 20,21 for FTP or Port 22 for sFTP.<br>

### 7. File Server
* A File Server is different from an FTP server. This type of server is more modern and is typically capable of “mapping” networked files onto drives.<br> 
* The main advantage of this form of server is that users can upload and download shared files.<br>
* Permissions to files are controlled by the administrator.<br>
* Usually File Servers exist in corporate networks in a Windows Active Directory environment or in Linux environments.<br>

### 8. DHCP Server
* A DHCP Server uses the Dynamic Host Communication Protocol (DHCP) to configure the network settings of client computers.<br>
* Instead of having to manually configure static IP address and other network settings to client computers in a large network, a DHCP server in the network configures dynamically these network settings to LAN computers.<br>
* Port used for DHCP Servers: Port UDP 67.

---

# Public Address
* Class A 0.0.0.0-127.0.0.0
* Class B 128 .0.0.0-191.0.0.0
* Class C 192.0.0.0-223.0.0.0

# Types of Computer Network
Local Area Network (LAN): Connects devices in a limited geographical area, such as floor, building <br>
Wide Area Network (WAN): Connects devices in large geographical areas such as countries, cities <br>

## Network Management Models
It a representation of how data is manaage, and how applications are hosted in a network. <br>

## Two types of Models
Client-to-server and peer-to-peer <br>

# Subnet
* Subnet is a network inside network
* Subnet creates multiples logical network with different ranges of IP addresses
* Classless Inter-Domain Routing (CIDR) notation is used to specify subnet IP address ranges
* Devices that are in the same subnet can communicate with each other without using any routers

## Purpose of subnetting
* Isolate different parts of the network
* Apply different levels of security to different parts of the network
* Relieve congestion on the network
* A subnet mask defines which section of the address identifies the network, and which section identifies the hosts
* A router or default gateway is used by devices on one network to communicate with the devices in another network

A subnet is a segment of a VPC address range. You can launch AWS services in a subnet.
* CIDR blocks define subnets
* AWS reserves the first four IP addresses and the last IP address of every subnet for internal networking purposes
* A public subnet is accessible from within the VPC and from the internet
* A private subnet is only accessible from within the VPC, and it is not accessible from the internet

## Common network utilities
There are a few common network utilities that you can use to quickly troubleshoot network issues. These tools can help ensure uninterrupted service and prevent long delays

* ping tests connectivity. This tool tests if the remote device (server, desktop) is on the network
* nslookup queries the DNS and its servers. It shows the IP addresses that are associated with a given domain name
* traceroute enables users to see the networking path used. It is helpful for troubleshooting connectivity problems
* telnet is used for service response. This tool tests if the service that runs on the remote device is responding to requests

## Amazon Virtual Private Cloud
Amazon Virtual Private Cloud (Amazon VPC) is a service that you can use to provision a logically isolated section of the AWS Cloud, which is called a virtual private cloud.

A VPC enables you to:
* Enables you to customize its network configuration
* Enables you to use multiple layers of security
* Gives you control over your virtual networking resources
* Enables you to create a private network in the AWS Cloud that uses many of the same concepts and constructs as an on-premises network



## PACKET TRACER - Network Topology

### Steps to create a network in Packet Tracer


### Step 1: Create the network topology<br>

1. Add PCs, switches and routers
2. Use copper straight-through cable to connect PC to switch and switch to router. Connect routers using copper crossover cable

<br><br>

![Network Topology](/Networking_images/NetworkTopology.png)

<br><br>

### Step 2: Configure PC ip address and gateway ip address<br>

<br>

1. Click on the PC to which you need to assign the ip address
2. Click on the Desktop and click ip configuration. Assign ip address, subnest mask and Default gateway.

<br>

> PC0 - 192.168.1.2, gateway - 192.168.1.1, network - 192.168.1.0 <br>
> PC1 - 192.168.1.3, gateway - 192.168.1.1, network - 192.168.1.0 <br>
> PC2 - 192.168.2.2, gateway - 192.168.2.1, network - 192.168.2.0 <br>
> PC3 - 192.168.2.3, gateway - 192.168.2.1, network - 192.168.2.0 <br>
> PC4 - 192.168.3.3, gateway - 192.168.3.1, network - 192.168.3.0 <br>
> PC5 - 192.168.4.2, gateway - 192.168.4.1, network - 192.168.4.0 <br>
> PC6 - 192.168.4.3, gateway - 192.168.4.1, network - 192.168.4.0 <br>
> PC7 - 192.168.3.2, gateway - 192.168.3.1, network - 192.168.3.0 <br>

<br>


### Follow the example below

<img src="/Networking_images/PC0- ip config.png" width=400/><br>

<br>

### Step 3: Configure ip addresses on routers

> **Router0** <br>
> GigabitEthernet0/0 - 192.168.5.1<br>
> GigabitEthernet0/1 - 192.168.1.1<br>
> GigabitEthernet0/2 - 192.168.2.1<br>


> **Router1** <br>
> GigabitEthernet0/0 - 192.168.5.2<br>
> GigabitEthernet0/1 - 192.168.3.1<br>
> GigabitEthernet0/2 - 192.168.4.1<br>



### Rules to assign IP address to the router

1. All the LAN and WAN should be in different networks
2. Router Ethernet IP and LAN network assigned should be in the same network
3. Both the interfaces of routers facing each other should be in the same network
4. All the interfaces of the router should be in different networks



### Follow the below steps to configure the routers

1. Double-click on the router and click CLI
2. Enter the following commands for each interface on the router


> enable<br>
> config t<br>
> interface (interface type)(nterface number)<br>
> ip address (IP address)(subnet mask)<br>
> no shutdown <br>
> exit<br>

### Follow the example given below for all the interfaces of Router0 and Router1<br>

> **For Router0, interface GigabitEthernet0/0** <br>
> enable<br>
> config t<br>
> interface GigabitEthernet 0/0<br>
> ip address 192.168.5.1 255.255.255.0<br>
> no shutdown <br>
> exit<br>


### Step 3: Configure static routing for Routers
1. Click on the router and click CLI
2. Enter network address(destination network), subnet mask and next hop(next router in the path)
<br><br>

> **For Router0** <br>
> ip route 192.168.3.0 255.255.255.0 192.168.5.2<br>
> ip route 192.168.4.0 255.255.255.0 192.168.5.2<br>

<br><br>

<img src="/Networking_images/Router0.png" width=500/>
<br>
<br>
<br>

> **For Router1** <br>
> ip route 192.168.1.0 255.255.255.0 192.168.5.1<br>
> ip route 192.168.2.0 255.255.255.0 192.168.5.1<br>

<br><br>

<img src="/Networking_images/Router1.png" width=500/>

<br></br>

### Step 4: Verify connectivity

<br>

 * Use the ping command to verify end-to-end connectivity
 * Connectivity can also be tested between devices by using simple PDUs
